// Commbank grunt build

module.exports = function (grunt) {

	// load all grunt tasks

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// configurable paths

	var pkg = grunt.file.readJSON('package.json'),
		dev      = '.',
		build    = '../build',
		release  = '../../../compiled/components/';

	var jsFiles = [''];

	grunt.initConfig({
		banner: '/* <%= pkg.name %> - <%= pkg.version %> - <%= pkg.author %> */\n',
		pkg: grunt.file.readJSON('package.json'),
		dirs: {
			dev: dev,
			release: release,
			build: build
		},
		watch: {
			images: {
				files: ['<%= dirs.dev %>/images/**/*'],
				tasks: ['copy:build']
			},
			js: {
				files: ['<%= dirs.dev %>/js/**/*.js'],
				tasks: ['concat:build']
			},
			html: {
				files: ['<%= dirs.dev %>/partials/*.html', '<%= dirs.dev %>/templates/*.html'],
				tasks: ['includereplace:build']
			},
			sass: {
				files: ['<%= dirs.dev %>/sass/**/*'],
				tasks: ['compass:build']
			},
			fonts: {
				files: ['<%= dirs.dev %>/sass/**/*'],
				tasks: ['compass:build']
			}
		},
		compass: {
			dev: {
				options: {
					force: true,
					sassDir: dev + '/sass',
					cssDir: build + '/css'
				}
			},
			compiled: {
				options: {
					force: true,
					sassDir: dev + '/sass',
					cssDir: release + '/css',
					outputStyle: 'compressed'
				}
			}
		},
		copy: {
			build: {
				files: [
					{
						expand: true,
						cwd: '<%= dirs.dev %>/images/',
						src: ['**'],
						dest: '<%= dirs.release %>/images/'
					},
					{
						expand: true,
						cwd: '<%= dirs.dev %>/images/',
						src: ['**'],
						dest: '<%= dirs.build %>/images/'
					}
				]
			},
			release: {
				files: [
					{
						expand: true,
						cwd: '<%= dirs.dev %>/images/',
						src: ['**'],
						dest: '<%= dirs.release %>/images/'
					}
				]
			}
		},
		includereplace: {
			build: {
				options: {
					prefix: '<!-- @@',
					suffix: ' -->'
				},
				files: [
					{
						src: '*.html',
						dest: release,
						expand: true,
						cwd: dev + '/templates/'
					}
				]
			},
			release: {
				options: {
					prefix: '<!-- @@',
					suffix: ' -->'
				},
				files: [
					{
						src: '*.html',
						dest: build + '/templates/',
						expand: true,
						cwd: dev + '/templates/'
					}
				]
			}
		},
		bump: {
			options: {
				files: ['package.json'],
				commit: false,
				createTag: false,
				push: false
			}
		},
		clean: {
			build: {
				options: {
					force: true
				},
				src: ['<%= dirs.release %>']
			}
		},
		uglify: {
			options: {
				sourceMap: true,
				sourceMapName: '<%= dirs.build %>/js-maps/<%= pkg.name %>.map',
				compress: {
					drop_console: true
				},
				banner: '<%= banner %>'
			},
			js: {
				files: {
					'<%= dirs.release %>/js/<%= pkg.name %>.js': jsFiles
				}
			}
		},
		concat: {
			option : {
				separator: ';'
			},
			js: {
				src: jsFiles,
				dest: '<%= dirs.build %>/js/<%= pkg.name %>.js'
		 	}
		}
	});

	grunt.registerTask('build', ['clean', 'concat', 'copy:images', 'copy:css', 'copy:js', 'compass:dev', 'includereplace:dev']);
	//grunt build task controls version number, if it hits a 100 gets a minor version bump
	grunt.registerTask('release', ['copy:jsComp','copy:images', 'copy:css', 'uglify', 'compass:build', 'includereplace:build', 'usebanner']);



};

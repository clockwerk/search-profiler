
	var read = require('./modules/read.js'),
		rp = require('./modules/render-page.js'),
		cp = require('./modules/create-report.js');

	var pages1 = [
			{url : '10.38.202.161:7503/about-us.html'},
			{url : '10.38.202.161:7503/about-us/can-connect.html'},
			{url : '10.38.202.161:7503/about-us/careers.html'},
			{url : '10.38.202.161:7503/about-us/shareholders/securities/retail-bonds.html'},
			{url : '10.38.202.161:7503/about-us/who-we-are/customer-commitment/financial-hardship.html'},
			{url : '10.38.202.161:7503/blog/my-first-property-home-or-investment.html'},
//			{url : '10.38.202.161:7503/blog/newsletter/weekly/11-06-2014.html'},
			{url : '10.38.202.161:7503/business.html'},
			{url : '10.38.202.161:7503/business/business-accounts.html'},
			{url : '10.38.202.161:7503/business/can/business-insights/women-in-focus.html'},
			{url : '10.38.202.161:7503/business/contact-us.html'},
			{url : '10.38.202.161:7503/business/locate-us.html'},
			{url : '10.38.202.161:7503/business/merchant-services/device-and-plan-selector.html'},
			{url : '10.38.202.161:7503/business/merchant-services/eftpos-options/payments-in-your-shop.html'},
			{url : '10.38.202.161:7503/business/our-innovation.html'},
			{url : '10.38.202.161:7503/corporate.html'},
			{url : '10.38.202.161:7503/corporate/can/our-philosophy.html'},
			{url : '10.38.202.161:7503/corporate/industries/healthcare.html'},
			{url : '10.38.202.161:7503/personal.html'},
			{url : '10.38.202.161:7503/personal/accounts.html'},
			{url : '10.38.202.161:7503/personal/can.html'},
			{url : '10.38.202.161:7503/personal/can/buying-a-car.html'},
			{url : '10.38.202.161:7503/personal/can/buying-a-home.html'},
			{url : '10.38.202.161:7503/personal/can/mega_nav.html'},
			{url : '10.38.202.161:7503/personal/can/moving-to-australia.html'},
			{url : '10.38.202.161:7503/personal/can/superannuation-smarts.html'},
			{url : '10.38.202.161:7503/personal/credit-cards.html'},
			{url : '10.38.202.161:7503/personal/credit-cards/awards/awards.html'},
			{url : '10.38.202.161:7503/personal/credit-cards/low-rate.html'},
			{url : '10.38.202.161:7503/personal/credit-cards/low-rate/low-rate.html'},
			{url : '10.38.202.161:7503/personal/home-loans.html'},
			{url : '10.38.202.161:7503/personal/home-loans/buying-a-home.html'},
			{url : '10.38.202.161:7503/personal/home-loans/loan-calculator.html'},
			{url : '10.38.202.161:7503/personal/login-netbank.html'},
			{url : '10.38.202.161:7503/personal/online-banking/netbank.html'},
			{url : '10.38.202.161:7503/personal/personal-loans/fixed-rate-loan/rates-and-fees.html'},
			{url : '10.38.202.161:7503/personal/products.html'},
			{url : '10.38.202.161:7503/personal/support.html'},
			{url : '10.38.202.161:7503/personal/support/contact-us.html'},
			{url : '10.38.202.161:7503/personal/tools.html'},
			{url : '10.38.202.161:7503/support/faqs/622.html'},
			{url : '10.38.202.161:7503/support/faqs/9762.html'},
			{url : '10.38.202.161:7503/support/topics/log-on-to-netbank-and-other-online-services.html'},
			{url : '10.38.202.161:7503/business/merchant-services/help-me-choose.html'},
			{url : '10.38.202.161:7503/business/merchant-services/help-me-choose-tool.html##slide_1'}
		],

	_analysisComplete = function(pages){

		cp.generateCSV(pages);

	},

	_readUrlsComplete = function(pages){

		rp.pageRender(pages, _analysisComplete);

	};

	read.readUrls(pages1, _readUrlsComplete);






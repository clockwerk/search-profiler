/**
 * Created by danielpeterson on 27/08/2014.
 */

var phantom = require('phantom');

var _highlightTags = function (run) {

		var all = false;

		function traverse (nodes, depth, gOff, gOn, gColor, bColor, cColor) {

			for (var i = 0; i < nodes.length; i++) {
				if (nodes[i].nodeName == "SCRIPT") {
					continue;
				}
				if (nodes[i].nodeName == "STYLE") {
					continue;
				}
				if (nodes[i].nodeName == "#text") {
					continue;
				}
				if (nodes[i].nodeName == "#comment") {

					if (nodes[i].nodeValue == gOff) {

						all = true;

					} else if (nodes[i].nodeValue == gOn) {

						all = false;

					}

				}

				var style = nodes[i].style;

				if (style) {

					if (all) {

						style.setProperty("color", bColor, "important");

					} else if (!all) {

						if (nodes[i].style.color !== 'rgb(33, 219, 0)') {

							style.setProperty("color", gColor, "important");

						}

					}

				}

				if (nodes[i].childNodes) {
					traverse(nodes[i].childNodes, depth + 1, gOff, gOn, gColor, bColor, cColor);
				}

			}

		}

		if (run === 1) {
			traverse(document.body.childNodes, 0, 'googleoff: all', 'googleon: all', "#21DB00", "#E80055", 'purple');
		} else {
			traverse(document.body.childNodes, 0, 'googleoff: snippet', 'googleon: snippet', "#E80055", "orange", 'purple');
		}

	},

	_countTags = function () {

		var h1, h2, metaDesc, metaKey;

		var tags = [

				h1 = {
					name: 'h1',
					selector: document.querySelectorAll('h1'),
					type: 'text'
				},
				h2 = {
					name: 'h2',
					selector: document.querySelectorAll('h2'),
					type: 'text'
				},
				metaDesc = {
					name: 'metaDesc',
					selector: document.querySelectorAll('meta[name="Description"]'), //document.querySelectorAll('meta[name="Description"]')[0]
					type: 'content'
				},
				metaKey = {
					name: 'metaKey',
					selector: document.querySelectorAll('meta[name="keywords"]'),
					type: 'content'
				}

			],

			_tagCheck = function (tag) {

				tag.lngth = tag.selector.length;

				if (tag.lngth > 1) {

					tag.content = [];

					var l = tag.lngth;

					for (var j = 0; j < l; j++) {

						if (tag.type === 'text') {

							var contentText = '<' + tag.name + '>' + tag.selector[j].innerHTML + '</' + tag.name + '>' || 'none';

							tag.content.push(contentText);

						}
						else {

							var contentContent = tag.selector[j].getAttribute("content") || 'none';

							tag.content.push(contentContent);

						}

					}

				} else if (tag.lngth !== 0) {

					if (tag.type === 'text') {

						tag.content = '<' + tag.name + '>' + tag.selector[0].innerHTML + '</' + tag.name + '>' || 'none';

					}
					else {

						tag.content = tag.selector[0].getAttribute("content") || 'none';

					}

				} else {

					tag.content = 'No Tags';

				}

				return tag;

			};

		var l = tags.length, result = {};

		for (var j = 0; j < l; j++) {

			result[tags[j].name] = _tagCheck(tags[j]);

			delete result[tags[j].name].selector;

		}

		return result;

	};


var i = 0;

exports.pageRender = function (pages, callback) {

	var _highLightIndexRoundTwo = function (ph, page) {

			page.evaluate(_highlightTags, function () {



//				console.log(pages[i].url + ' Index Highlighting Applied to Page');

//				console.log('---------------------------------------');


				i = i + 1;

				_renderPage(ph, page);

			}, 2);

		},

		_pageTagCount = function (ph, page) {

			page.evaluate(_countTags, function (result) {

				pages[i].tags = result;

//				console.log(pages[i].url + ' Tags Counted');

				page.render('../output/images/' + pages[i].name + '.jpg', {format: 'jpeg', quality: '80'}, function () {

//					console.log(pages[i].url + ' Image Snapshot Taken');

					_highLightIndexRoundTwo(ph, page)

				});

			});
		},


		_hightLightIndex = function (ph, page) {

			page.evaluate(_highlightTags, function () {

				_pageTagCount(ph, page);

//				console.log(pages[i].url + ' Index Highlighting Applied to Page');

			}, 1);

		},

		_renderPage = function (ph, page) {

			if (i !== pages.length) {
				console.log(i);
				page.open(pages[i].url, function (status) {

//					console.log(pages[i].url + ' ' + status);

					_hightLightIndex(ph, page);

				});

			}
			else {

				ph.exit();

				if (callback) callback(pages);

			}

		};

	phantom.create('--ignore-ssl-errors=true', function (ph) {

		console.log(ph);

		ph.createPage(function (page) {

			page.set('viewportSize', {width: 1140, height: 1000});

			_renderPage(ph, page);

		});

	});

};






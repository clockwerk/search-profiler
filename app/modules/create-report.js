
	var fs = require('fs');
	var chalk = require('chalk');

	exports.generateCSV = function (pages) {

		var _createString = function (page) {

			var _stringifyArray = function (array) {

				var string = '';

				if (typeof array === 'object') {

					array.forEach(function (item) {

						string += item;

					});

					return string;

				}
				else {

					return array;

				}

			};

			page.tags.h1.content = _stringifyArray(page.tags.h1.content);

			page.tags.h2.content = _stringifyArray(page.tags.h2.content);

			page.tags.metaDesc.content = _stringifyArray(page.tags.metaDesc.content);

			page.tags.metaKey.content = _stringifyArray(page.tags.metaKey.content);


			return '<tr>' +
					'<th>Name</th>' +
					'<th>URL</th>' +
					'<th>H1</th>' +
					'<th>H1 Content</th>' +
					'<th>H2</th>' +
					'<th>H2 Content</th>' +
					'<th>Meta Description</th>' +
					'<th>Meta Description Content</th>' +
					'<th>Meta Keywords</th>' +
					'<th>Meta Keywords Content</th>' +
				'</tr>' +
				'<tr>' +
					'<td>' + page.name + '</td>' +
					'<td>' + page.url + '</td>' +
					'<td>' + page.tags.h1.lngth + '</td>' +
					'<td>' + page.tags.h1.content +'</td>' +
					'<td>' + page.tags.h2.lngth +'</td>' +
					'<td>' + page.tags.h2.content +'</td>' +
					'<td>' + page.tags.metaDesc.lngth +'</td>' +
					'<td>' + page.tags.metaDesc.content +'</td>' +
					'<td>' + page.tags.metaKey.lngth +'</td>' +
					'<td>' + page.tags.metaKey.content + '</td>' +
				'</tr>' +
				'<tr>' +
					'<td colspan="10"><img src="images/' + page.name + '.png"></td>' +
				'</tr>';

		},

		_createFile = function (content) {

			var htmlHeader =    '<!doctype html>' +
						        '<head>' +
							    '<style type="text/css">' +
						        'table {' +
						        'text-align:left;' +
								'font-family: helvetica, arial;' +
							    '}' +
								'table * {vertical-align:top; font-size:14px;}' +
						        '</style>' +
								'<script src="http://code.jquery.com/jquery-2.1.1.js"></script>' +
							    '</head>' +
							    '<body><table cellspacing="0" cellpadding="5" border="1">',

				htmlfooter = '</table>' + '</body>',

				page = htmlHeader + content + htmlfooter;

			_publishCSV(page);

		},

		_publishCSV = function (page) {

			fs.writeFile('../output/sitedata.html', page, function(){

				console.log(chalk.green('sitedata.html created'));

			});

		};

		var pl = pages.length, content = '';

		for (var i = 0; i < pl; i++) {

			content += _createString(pages[i]);

		}

		_createFile(content);

	};


